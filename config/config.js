const configDB = {
    dbHost: "wfh-gcp-mongodb.net",
    dbPort: "27017",
    dbName: "discord-checkin"
}

module.exports = {
    configDB
}
